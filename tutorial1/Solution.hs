module Solution where

import Test.QuickCheck
import Data.Char

-- | QuickCheck
absolute :: Int -> Int
absolute x = if x < 0 then -x else x

{- The process of testing a property with a premise:
   1. Generate a new test case
   2. Check whether the premise holds:
      a. Yes. Test the property
      b. No. Abort the test case
   3. Goto 1.

   Note:
   1. Format is premise ==> property
   2. Return type is *Property* instead of Bool
   3. By default, 100 test cases are generated *including* those aborted.
      You can run `verboseCheck prop` instead of `quickCheck prop` to see the details.
 -}
prop_absolute :: Int -> Int -> Property
prop_absolute a b =
    let a' = toInteger a
        b' = toInteger b
        maxInt = toInteger (maxBound :: Int)
        minInt = toInteger (minBound :: Int)
    in
      (a' * b' < maxInt && a' * b' > minInt) ==> (absolute a) * (absolute b) == absolute (a * b)

prop_absolute' :: Int -> Int -> Property
prop_absolute' a b =
      (a' * b' < maxInt && a' * b' > minInt) ==> (absolute a) * (absolute b) == absolute (a * b)
    where a' = toInteger a
          b' = toInteger b
          maxInt = toInteger (maxBound :: Int)
          minInt = toInteger (minBound :: Int)

-- | Type inference
newline s = s ++ "\n"
identity x = x
pair x y = (x,y)

{- Although haskell will infer the type for you, you are strongly recommended to write the type signature explicitly beforehand for two purpose:
   1. Make yourself clear when implementing the function.
   2. Serve as the document. Combined with the name of function, people can guess the behavior of the function.
   It is useful to compare your type signature with the infered one to see whether the function you defined can be more general.
 -}

-- | Pattern matching
hd        :: [a] -> a
hd []     =  error "cannot take the head of an empty list!"
hd (x:xs) = x

tl :: [a] -> [a]
tl []      = error  "cannot take the tail of an empty list!"
tl (x:xs)  = xs

negate :: Bool -> Bool
negate True = False
negate False = True

isPi :: Double -> Bool
isPi 3.14159 = True
isPi _ = False
-- Note: '_' is the wildcard pattern. Pattern matching will always succeed for this pattern.

-- | Recursion
{- Two elements of recursion that ensure it terminates:
   1. Base case.
      e.g. [] is the base case for lists
   2. Smaller input for the next recursive call.
      e.g. apply sumList to xs where xs is smaller than (x:xs)
 -}
sumList :: [Int] -> Int
sumList []      = 0
sumList (x:xs)  = x + sumList xs

factorial :: Int -> Int
factorial 0 = 1
factorial n = n * factorial (n-1)

fibonacci :: Int -> Int
fibonacci 0 = 0
fibonacci 1 = 1
fibonacci n = fibonacci (n-1) + fibonacci (n-2)

mapList :: (a -> b) -> [a] -> [b]
mapList f []      = []
mapList f (x:xs)  = f x : mapList f xs

ascii    :: [Char] -> [Int]
ascii xs = mapList ord xs

zipList :: [a] -> [b] -> [(a,b)]
zipList []     ys     = []
zipList xs     []     = []
zipList (x:xs) (y:ys) = (x,y) : zipList xs ys

zipList' :: [a] -> [b] -> [(a,b)]
zipList' (x:xs) (y:ys) = (x,y) : zipList xs ys
zipList' xs     ys     = []

add :: (Int,Int) -> Int
add (x,y) = x + y

zipSum :: [Int] -> [Int] -> [Int]
zipSum xs ys = map add (zipList xs ys)

zipSum' :: [Int] -> [Int] -> [Int]
zipSum' xs ys = map add (zipList xs ys)
    where add :: (Int,Int) -> Int
          add (x,y) = x + y

-- If you don't want to name the function or the function can not be used elsewhere, then define it as a lambda.
zipSum'' :: [Int] -> [Int] -> [Int]
zipSum'' xs ys = map (\(x,y) -> x + y) (zipList xs ys)

-- Further refactoring: use wildcard to replace uninteresting variable names
